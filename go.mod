module gitlab.com/juavabar/client-go-18

go 1.12

// ORIGINAL REQUIRE: (using clusters kubernetes v1.16.10)
// require (
//   github.com/sirupsen/logrus v1.6.0
//   k8s.io/client-go    kubernetes-1.16.10
//   k8s.io/apimachinery kubernetes-1.16.10
// )

require (
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/kumori-systems/community/libraries/client-go v0.1.13 // indirect
	k8s.io/apimachinery v0.18.3
	k8s.io/client-go v0.18.3
	k8s.io/code-generator v0.18.6-rc.0 // indirect
)
