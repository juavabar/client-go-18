/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuHttpInbound is a top-level type
type KukuHttpInbound struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              KukuHttpInboundSpec `json:"spec"`
}

// Cert is part of the KukuHttpInboundSpec
type Cert struct {
	Domain   string `json:"domain"`
	KukuCert string `json:"kukucert"`
}

// Node is part of the KukuHttpInboundSpec
type Node struct {
	IP string `json:"ip"`
}

// KukuHttpInboundSpec is the custom spec of KukuHttpInbound
type KukuHttpInboundSpec struct {
	TLS        bool     `json:"tls"`
	ClientCert bool     `json:"clientcert"`
	Websocket  *bool    `json:"websocket"`
	VHosts     []string `json:"vhosts"`
	Certs      []Cert   `json:"certs"`
	Nodes      []Node   `json:"nodes"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuHttpInboundList is a list of KukuHttpInbound resources
type KukuHttpInboundList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuHttpInbound `json:"items"`
}
