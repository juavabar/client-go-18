/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuLink is a top-level type
type KukuLink struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              KukuLinkSpec `json:"spec"`
}

// KukuLinkSpec is the custom spec of KukuLink
type KukuLinkSpec struct {
	Endpoints []KukuEndpoint `json:"endpoints"`
}

// KukuEndpoing contains the deployment and channel of one side of the link
type KukuEndpoint struct {
	Name    string `json:"name"`
	Kind    string `json:"kind"`
	Domain  string `json:"domain"`
	Channel string `json:"channel"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KukuLinkList is a list of KukuLink resources
type KukuLinkList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KukuLink `json:"items"`
}
