/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// V3Deployment is a top-level type
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
type V3Deployment struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// Spec describes the desired service.
	// +optional
	Spec DeploymentSpec `json:"spec,omitempty"`

	// Status is the current status of this service. This data
	// may be out of date by some window of time.
	// +optional
	Status DeploymentStatus `json:"status,omitempty"`
}

// DeploymentSpec is the custom spec of Deployment
type DeploymentSpec struct {
	Element     `json:",inline"`
	Description *DeploymentDescription `json:"description"`
}

// DeploymentStatus stores information about the observed status of a V3Deployment
type DeploymentStatus struct {

	// observedGeneration is the most recent generation observed for this V3Deployment. It corresponds to // the Deployment generation, which is updated on mutation by the API Server.
	// +optional
	ObservedGeneration int64 `json:"observedGeneration,omitempty"`

	// currentRevision is the name of the ControllerRevision representing the current state.
	CurrentRevision string `json:"currentRevision,omitempty"`

	// collisionCount is the count of hash collisions for the V3Deployment. The V3Deployment controller
	// uses this field as a collision avoidance mechanism when it needs to create the name for the
	// newest ControllerRevision.
	// +optional
	CollisionCount *int32 `json:"collisionCount,omitempty" protobuf:"varint,9,opt,name=collisionCount"`
}

// Element is blah
type Element struct {
	Ref   Reference `json:"ref"`
	Scope *Scope    `json:"scope,omitempty"`
	Spec  Version   `json:"spec"`
}

// Reference is blah
type Reference struct {
	Name    Name    `json:"name"`
	Kind    RefKind `json:"kind"`
	Domain  Domain  `json:"domain"`
	Version Version `json:"version"`
}

// DeploymentDescription is blah
type DeploymentDescription struct {
	Service *Service `json:"service"`
	Qos     *Qos     `json:"QoS,omitempty"`
}

// Service is blah
type Service struct {
	Element     `json:",inline"`
	Description *ServiceDescription `json:"description"`
}

// ServiceDescription is blah
type ServiceDescription struct {
	Config    Config               `json:"config"`
	Srv       Srv                  `json:"srv"`
	Roles     map[string]Role      `json:"role"`
	Connector map[string]Connector `json:"connector"`
}

// Connector contains information of a given service connector
type Connector struct {
	Kind    ConnectorType       `json:"kind"`
	Clients []ConnectorEndpoint `json:"clients"`
	Servers []ConnectorTag      `json:"servers"`
}

// ConnectorTag contains information about an available tagged group of targets
type ConnectorTag struct {
	Meta  *runtime.RawExtension `json:"meta"`
	Links []ConnectorEndpoint   `json:"links"`
}

// ConnectorEndpoint describes a source/target endpoint
type ConnectorEndpoint struct {
	Role    string `json:"role"`
	Channel string `json:"channel"`
}

// ConnectorType contains the type of a connector
type ConnectorType string

const (
	// LBConnectorType is blah
	LBConnectorType ConnectorType = "lb"
	// FullConnectorType is blah
	FullConnectorType ConnectorType = "full"
)

// Role is blah
type Role struct {
	Component *Component  `json:"component"`
	Size      RoleSize    `json:"rsize"`
	Updates   UpdatesType `json:"updates"`
}

// Component is blah
type Component struct {
	Element     `json:",inline"`
	Description *ComponentDescription `json:"description"`
}

// ProbeType is the type of a declared probe
type ProbeType string

const (
	// LivenessProbe is a liveness probe
	LivenessProbe ProbeType = "liveness"
	// PrometheusMetricsProbe is a probe to get metrics for Prometheus
	PrometheusMetricsProbe ProbeType = "pmetrics"
)

// ComponentDescription describes a role's component, including:
// * Config: the component configuration and assigned resources
// * Srv: the component client, server and duplex channels.
// * Profile: the component characteristics
// * Codes: the component containers.
// * Probes: the component probes per probe type and container.
type ComponentDescription struct {
	Config  Config                          `json:"config"`
	Srv     Srv                             `json:"srv"`
	Size    ComponentSize                   `json:"size"`
	Profile Profile                         `json:"profile"`
	Codes   map[string]Code                 `json:"code"`
	Probes  *map[ProbeType]map[string]Probe `json:"probe,omitempty"`
}

// Probe is a probe to check the component status. Expected source format:
// "probe": {
//   "liveness": {
//     "frontend": {
//       "kind": "liveness",
//       "attributes": {
//         "protocol": "http",
//         "startupGraceWindow": {
//           "unit": "attempts",
//           "duration": 5
//         },
//         "attributes": {
//           "path": "/getenv"
//         }
//       },
//       "port": 8080
//     }
//   }
// },
// The `Attributes` content will depend on the `K   ind`:
// * `TCPLivenessProbe`: TCPLivenessProbeAttributes
// * `HTTPLivenesProbe`: HTTPLivenessProbeAttributes
// * `PrometheusMetricsProbe`: PrometheusMetricsProbeAttributes
type Probe struct {
	Kind       ProbeType             `json:"kind"`
	Port       uint16                `json:"port"`
	Attributes *runtime.RawExtension `json:"attributes,omitempty"`
}

// LivenessProbeProtocolType is the protocol used in the liveness probe
type LivenessProbeProtocolType string

const (
	// HTTPLivenessProbeProtocolType is the HTTP protocol
	HTTPLivenessProbeProtocolType LivenessProbeProtocolType = "http"
	// TCPLivenessProbeProtocolType is the TCP protocol
	TCPLivenessProbeProtocolType LivenessProbeProtocolType = "tcp"
)

// LivenessProbeAttributes are the attributes required by liveness probe
type LivenessProbeAttributes struct {
	Protocol           LivenessProbeProtocolType `json:"protocol"`
	StartupGraceWindow StartupGraceWindow        `json:"startupGraceWindow"`
	Attributes         *runtime.RawExtension     `json:"attributes,omitempty"`
}

// StartupGraceWindow defines the maximum duration of a componen startup. It can
// be represented as an absolute time or as a number of retries.
type StartupGraceWindow struct {
	Unit     StartupGraceWindowUnit `json:"unit"`
	Duration uint                   `json:"duration"`
}

// StartupGraceWindowUnit the units used to represent the maximum startup
// duration for a given components. Can be "attempts" or "ms"
type StartupGraceWindowUnit string

const (
	// AttemptsStartupGraceWindowUnit is used when the duration is represented in attempts
	AttemptsStartupGraceWindowUnit StartupGraceWindowUnit = "attempts"
	// MillisStartupGraceWindowUnit is used when the duration is represented in millis
	MillisStartupGraceWindowUnit StartupGraceWindowUnit = "ms"
)

// TCPLivenessProbeAttributes are the attributes required by a TCP liveness probe
type TCPLivenessProbeAttributes struct{}

// HTTPLivenessProbeAttributes are the attributes required by a HTTP liveness probe
type HTTPLivenessProbeAttributes struct {
	Path string `json:"path"`
}

// PrometheusMetricsProbeAttributes are the attributes required by a Prometheus metrics probe
type PrometheusMetricsProbeAttributes struct {
	Path string `json:"path"`
}

// ComponentSize is blah
type ComponentSize struct {
	CPU           string `json:"$_cpu"`
	Memory        string `json:"$_memory"`
	Ioperf        string `json:"$_ioperf"`
	Iopsintensive bool   `json:"$_iopsintensive"`
	Bandwidth     string `json:"$_bandwidth"`
}

// Profile is blah
type Profile struct {
	Threadability string `json:"threadability"`
}

// Code is blah
type Code struct {
	Name       string         `json:"name"`
	Image      Image          `json:"image"`
	Entrypoint *[]string      `json:"entrypoint,omitempty"`
	Mapping    *Mapping       `json:"mapping,omitempty"`
	Size       *ComponentSize `json:"size,omitempty"`
}

// Mapping is blah
type Mapping struct {
	Env        *map[string]Env `json:"env,omitempty"`
	FileSystem *[]MountPoint   `json:"filesystem,omitempty"`
}

// Env contains the environment variable value or the secret storing this value.
type Env struct {
	Value  *string `json:"value,omitempty"`
	Secret *string `json:"secret,omitempty"`
}

// MountPoint contains information to expose data on a container filesystem. It can mount:
// * A file: the data will be taken from `Data` or from a Secret.
// * A volatlile empty volume: the size of the volume must be specified.
// * A persistent volume: TBD
type MountPoint struct {
	// Data contains the file content if the data is directly included.
	Data *runtime.RawExtension `json:"data,omitempty"`
	// Format contains information about the format of the file content. Formats allowed:
	// * text: the content is stored as it is.
	// * json: the content is a json document.
	// * yaml: the content is a yaml document.
	Format *string `json:"format,omitempty"`
	// Group contains the group owning this file in the container
	Group *int64 `json:"group,omitempty"`
	// Mode contains the file permissions
	Mode *int32 `json:"mode,omitempty"`
	// Path indicates where the file should be stored
	Path string `json:"path"`
	// Secret indicates the name of the Secret resource storing the data content
	Secret *string `json:"secret,omitempty"`
	// Sizs contains the volume size if the element mounted is a volume
	Size *int32 `json:"size,omitempty"`
	// Unit contains the unit of the Size if the element mounted is a volume. The default value is `G`
	Unit *string `json:"unit,omitempty"`
	// User contains the user owning this file in the container.
	User *int64 `json:"user,omitempty"`
	// RebootOnUpdate must set to true to disable file hot updates. Hence, if the file data changes, the
	// role instances is rebooted.
	RebootOnUpdate *bool `json:"rebootOnUpdate,omitempty"`
}

// Image is blah
type Image struct {
	Hub *Hub   `json:"hub,omitempty"`
	Tag string `json:"tag"`
}

// Hub is blah
type Hub struct {
	Name   string  `json:"name"`
	Secret *string `json:"secret,omitempty"`
}

// RoleSize is blah
type RoleSize struct {
	Resilience   int32 `json:"$_resilience"`
	Instances    int32 `json:"$_instances"`
	MaxInstances int32 `json:"$_maxinstances"`
}

// UpdatesType is blah
type UpdatesType string

const (
	// FreeUpdatesType is blah
	FreeUpdatesType UpdatesType = "free"
)

// Config is blah
type Config struct {
	Parameters *map[string]runtime.RawExtension `json:"parameter,omitempty"`
	Resource   *map[string]runtime.RawExtension `json:"resource,omitempty"`
}

// Srv is blah
type Srv struct {
	Duplex  *map[string]Duplex `json:"duplex,omitempty"`
	Clients *map[string]Client `json:"client,omitempty"`
	Servers *map[string]Server `json:"server,ompitempty"`
}

// Duplex is blah
type Duplex Server

// Client is blah
type Client struct {
	Protocol SrvProtocol `json:"protocol"`
}

// Server is blah
type Server struct {
	Protocol SrvProtocol `json:"protocol"`
	Port     int32       `json:"port"`
}

// SrvProtocol is blah
type SrvProtocol string

const (
	// HTTPSrvProtocol is blah
	HTTPSrvProtocol SrvProtocol = "http"
	// TCPSrvProtocol is blah
	TCPSrvProtocol SrvProtocol = "tcp"
)

// ToConnector is blah
type ToConnector struct {
	To string `json:"to"`
}

// ToChannel is blah
type ToChannel struct {
	To map[string]string `json:"to"`
}

// Qos is blah
type Qos struct{}

// Name is blah
type Name string

// RefKind is blah
type RefKind string

const (
	// DeploymentKind is blah
	DeploymentKind RefKind = "deployment"
	// ServiceKind is blah
	ServiceKind RefKind = "service"
)

// Domain is blah
type Domain string

// Version is blah
type Version []int16

// Scope is blah
type Scope []ScopeElement

// ScopeElement is blah
type ScopeElement string

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// V3DeploymentList is a list of V3Deployment resources
type V3DeploymentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []V3Deployment `json:"items"`
}
