/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// Our controllers works with Ambassador objects (CRDs), so we want generate
// code for them

// ---------------------------------------------------------------------------

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KubernetesEndpointResolver is a top-level type
type KubernetesEndpointResolver struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// KubernetesEndpointResolverList is a list of KubernetesEndpointResolver resources
type KubernetesEndpointResolverList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []KubernetesEndpointResolver `json:"items"`
}

// ---------------------------------------------------------------------------

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TLSContext is a top-level type
type TLSContext struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              TLSContextSpec `json:"spec"`
}

// TLSContextSpec is the custom spec of TLSContext
type TLSContextSpec struct {
	Hosts                 []string `json:"hosts"`
	Secret                string   `json:"secret"`
	CaSecret              string   `json:"ca_secret"`
	RedirectCleartextFrom int32    `json:"redirect_cleartext_from"`
	MinTLSVersion         string   `json:"min_tls_version,omitempty"`
	MaxTLSVersion         string   `json:"max_tls_version,omitempty"`
	CipherSuites          []string `json:"cipher_suites,omitempty"`
	ECDHCurves            []string `json:"ecdh_curves,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TLSContextList is a list of TLSContext resources
type TLSContextList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []TLSContext `json:"items"`
}

// ---------------------------------------------------------------------------

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Mapping is a top-level type
type Mapping struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              MappingSpec `json:"spec"`
}

// MappingSpec is the custom spec of Mapping
type MappingSpec struct {
	Host              string            `json:"host"`
	PrefixRegex       bool              `json:"prefix_regex"`
	Prefix            string            `json:"prefix"`
	Rewrite           string            `json:"rewrite"`
	Service           string            `json:"service"`
	Precedence        int               `json:"precedence"`
	Resolver          string            `json:"resolver"`
	LoadBalancer      map[string]string `json:"load_balancer"`
	Websocket         bool              `json:"use_websocket"`
	AddRequestHeaders map[string]string `json:"add_request_headers"`
	TimeoutMs         int               `json:"timeout_ms"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// MappingList is a list of Mapping resources
type MappingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []Mapping `json:"items"`
}

// ---------------------------------------------------------------------------

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TCPMapping is a top-level type
type TCPMapping struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              TCPMappingSpec `json:"spec"`
}

// TCPMappingSpec is the custom spec of TCPMapping
type TCPMappingSpec struct {
	Port     int    `json:"port"`
	Service  string `json:"service"`
	Resolver string `json:"resolver"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// TCPMappingList is a list of TCPMapping resources
type TCPMappingList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []TCPMapping `json:"items"`
}

// ---------------------------------------------------------------------------
