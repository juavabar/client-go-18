/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by informer-gen. DO NOT EDIT.

package v1

import (
	"context"
	time "time"

	kumoriv1 "gitlab.com/juavabar/client-go-18/pkg/apis/kumori/v1"
	versioned "gitlab.com/juavabar/client-go-18/pkg/generated/clientset/versioned"
	internalinterfaces "gitlab.com/juavabar/client-go-18/pkg/generated/informers/externalversions/internalinterfaces"
	v1 "gitlab.com/juavabar/client-go-18/pkg/generated/listers/kumori/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
	watch "k8s.io/apimachinery/pkg/watch"
	cache "k8s.io/client-go/tools/cache"
)

// V3DeploymentInformer provides access to a shared informer and lister for
// V3Deployments.
type V3DeploymentInformer interface {
	Informer() cache.SharedIndexInformer
	Lister() v1.V3DeploymentLister
}

type v3DeploymentInformer struct {
	factory          internalinterfaces.SharedInformerFactory
	tweakListOptions internalinterfaces.TweakListOptionsFunc
	namespace        string
}

// NewV3DeploymentInformer constructs a new informer for V3Deployment type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewV3DeploymentInformer(client versioned.Interface, namespace string, resyncPeriod time.Duration, indexers cache.Indexers) cache.SharedIndexInformer {
	return NewFilteredV3DeploymentInformer(client, namespace, resyncPeriod, indexers, nil)
}

// NewFilteredV3DeploymentInformer constructs a new informer for V3Deployment type.
// Always prefer using an informer factory to get a shared informer instead of getting an independent
// one. This reduces memory footprint and number of connections to the server.
func NewFilteredV3DeploymentInformer(client versioned.Interface, namespace string, resyncPeriod time.Duration, indexers cache.Indexers, tweakListOptions internalinterfaces.TweakListOptionsFunc) cache.SharedIndexInformer {
	return cache.NewSharedIndexInformer(
		&cache.ListWatch{
			ListFunc: func(options metav1.ListOptions) (runtime.Object, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.KumoriV1().V3Deployments(namespace).List(context.TODO(), options)
			},
			WatchFunc: func(options metav1.ListOptions) (watch.Interface, error) {
				if tweakListOptions != nil {
					tweakListOptions(&options)
				}
				return client.KumoriV1().V3Deployments(namespace).Watch(context.TODO(), options)
			},
		},
		&kumoriv1.V3Deployment{},
		resyncPeriod,
		indexers,
	)
}

func (f *v3DeploymentInformer) defaultInformer(client versioned.Interface, resyncPeriod time.Duration) cache.SharedIndexInformer {
	return NewFilteredV3DeploymentInformer(client, f.namespace, resyncPeriod, cache.Indexers{cache.NamespaceIndex: cache.MetaNamespaceIndexFunc}, f.tweakListOptions)
}

func (f *v3DeploymentInformer) Informer() cache.SharedIndexInformer {
	return f.factory.InformerFor(&kumoriv1.V3Deployment{}, f.defaultInformer)
}

func (f *v3DeploymentInformer) Lister() v1.V3DeploymentLister {
	return v1.NewV3DeploymentLister(f.Informer().GetIndexer())
}
