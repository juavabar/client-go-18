/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by client-gen. DO NOT EDIT.

package v1

import (
	"context"
	"time"

	v1 "gitlab.com/juavabar/client-go-18/pkg/apis/kumori/v1"
	scheme "gitlab.com/juavabar/client-go-18/pkg/generated/clientset/versioned/scheme"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	rest "k8s.io/client-go/rest"
)

// KukuLinksGetter has a method to return a KukuLinkInterface.
// A group's client should implement this interface.
type KukuLinksGetter interface {
	KukuLinks(namespace string) KukuLinkInterface
}

// KukuLinkInterface has methods to work with KukuLink resources.
type KukuLinkInterface interface {
	Create(ctx context.Context, kukuLink *v1.KukuLink, opts metav1.CreateOptions) (*v1.KukuLink, error)
	Update(ctx context.Context, kukuLink *v1.KukuLink, opts metav1.UpdateOptions) (*v1.KukuLink, error)
	Delete(ctx context.Context, name string, opts metav1.DeleteOptions) error
	DeleteCollection(ctx context.Context, opts metav1.DeleteOptions, listOpts metav1.ListOptions) error
	Get(ctx context.Context, name string, opts metav1.GetOptions) (*v1.KukuLink, error)
	List(ctx context.Context, opts metav1.ListOptions) (*v1.KukuLinkList, error)
	Watch(ctx context.Context, opts metav1.ListOptions) (watch.Interface, error)
	Patch(ctx context.Context, name string, pt types.PatchType, data []byte, opts metav1.PatchOptions, subresources ...string) (result *v1.KukuLink, err error)
	KukuLinkExpansion
}

// kukuLinks implements KukuLinkInterface
type kukuLinks struct {
	client rest.Interface
	ns     string
}

// newKukuLinks returns a KukuLinks
func newKukuLinks(c *KumoriV1Client, namespace string) *kukuLinks {
	return &kukuLinks{
		client: c.RESTClient(),
		ns:     namespace,
	}
}

// Get takes name of the kukuLink, and returns the corresponding kukuLink object, and an error if there is any.
func (c *kukuLinks) Get(ctx context.Context, name string, options metav1.GetOptions) (result *v1.KukuLink, err error) {
	result = &v1.KukuLink{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("kukulinks").
		Name(name).
		VersionedParams(&options, scheme.ParameterCodec).
		Do(ctx).
		Into(result)
	return
}

// List takes label and field selectors, and returns the list of KukuLinks that match those selectors.
func (c *kukuLinks) List(ctx context.Context, opts metav1.ListOptions) (result *v1.KukuLinkList, err error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	result = &v1.KukuLinkList{}
	err = c.client.Get().
		Namespace(c.ns).
		Resource("kukulinks").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Do(ctx).
		Into(result)
	return
}

// Watch returns a watch.Interface that watches the requested kukuLinks.
func (c *kukuLinks) Watch(ctx context.Context, opts metav1.ListOptions) (watch.Interface, error) {
	var timeout time.Duration
	if opts.TimeoutSeconds != nil {
		timeout = time.Duration(*opts.TimeoutSeconds) * time.Second
	}
	opts.Watch = true
	return c.client.Get().
		Namespace(c.ns).
		Resource("kukulinks").
		VersionedParams(&opts, scheme.ParameterCodec).
		Timeout(timeout).
		Watch(ctx)
}

// Create takes the representation of a kukuLink and creates it.  Returns the server's representation of the kukuLink, and an error, if there is any.
func (c *kukuLinks) Create(ctx context.Context, kukuLink *v1.KukuLink, opts metav1.CreateOptions) (result *v1.KukuLink, err error) {
	result = &v1.KukuLink{}
	err = c.client.Post().
		Namespace(c.ns).
		Resource("kukulinks").
		VersionedParams(&opts, scheme.ParameterCodec).
		Body(kukuLink).
		Do(ctx).
		Into(result)
	return
}

// Update takes the representation of a kukuLink and updates it. Returns the server's representation of the kukuLink, and an error, if there is any.
func (c *kukuLinks) Update(ctx context.Context, kukuLink *v1.KukuLink, opts metav1.UpdateOptions) (result *v1.KukuLink, err error) {
	result = &v1.KukuLink{}
	err = c.client.Put().
		Namespace(c.ns).
		Resource("kukulinks").
		Name(kukuLink.Name).
		VersionedParams(&opts, scheme.ParameterCodec).
		Body(kukuLink).
		Do(ctx).
		Into(result)
	return
}

// Delete takes name of the kukuLink and deletes it. Returns an error if one occurs.
func (c *kukuLinks) Delete(ctx context.Context, name string, opts metav1.DeleteOptions) error {
	return c.client.Delete().
		Namespace(c.ns).
		Resource("kukulinks").
		Name(name).
		Body(&opts).
		Do(ctx).
		Error()
}

// DeleteCollection deletes a collection of objects.
func (c *kukuLinks) DeleteCollection(ctx context.Context, opts metav1.DeleteOptions, listOpts metav1.ListOptions) error {
	var timeout time.Duration
	if listOpts.TimeoutSeconds != nil {
		timeout = time.Duration(*listOpts.TimeoutSeconds) * time.Second
	}
	return c.client.Delete().
		Namespace(c.ns).
		Resource("kukulinks").
		VersionedParams(&listOpts, scheme.ParameterCodec).
		Timeout(timeout).
		Body(&opts).
		Do(ctx).
		Error()
}

// Patch applies the patch and returns the patched kukuLink.
func (c *kukuLinks) Patch(ctx context.Context, name string, pt types.PatchType, data []byte, opts metav1.PatchOptions, subresources ...string) (result *v1.KukuLink, err error) {
	result = &v1.KukuLink{}
	err = c.client.Patch(pt).
		Namespace(c.ns).
		Resource("kukulinks").
		Name(name).
		SubResource(subresources...).
		VersionedParams(&opts, scheme.ParameterCodec).
		Body(data).
		Do(ctx).
		Into(result)
	return
}
