/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by lister-gen. DO NOT EDIT.

package v1

import (
	v1 "gitlab.com/juavabar/client-go-18/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// KukuPersistentVolumeLister helps list KukuPersistentVolumes.
type KukuPersistentVolumeLister interface {
	// List lists all KukuPersistentVolumes in the indexer.
	List(selector labels.Selector) (ret []*v1.KukuPersistentVolume, err error)
	// KukuPersistentVolumes returns an object that can list and get KukuPersistentVolumes.
	KukuPersistentVolumes(namespace string) KukuPersistentVolumeNamespaceLister
	KukuPersistentVolumeListerExpansion
}

// kukuPersistentVolumeLister implements the KukuPersistentVolumeLister interface.
type kukuPersistentVolumeLister struct {
	indexer cache.Indexer
}

// NewKukuPersistentVolumeLister returns a new KukuPersistentVolumeLister.
func NewKukuPersistentVolumeLister(indexer cache.Indexer) KukuPersistentVolumeLister {
	return &kukuPersistentVolumeLister{indexer: indexer}
}

// List lists all KukuPersistentVolumes in the indexer.
func (s *kukuPersistentVolumeLister) List(selector labels.Selector) (ret []*v1.KukuPersistentVolume, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuPersistentVolume))
	})
	return ret, err
}

// KukuPersistentVolumes returns an object that can list and get KukuPersistentVolumes.
func (s *kukuPersistentVolumeLister) KukuPersistentVolumes(namespace string) KukuPersistentVolumeNamespaceLister {
	return kukuPersistentVolumeNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// KukuPersistentVolumeNamespaceLister helps list and get KukuPersistentVolumes.
type KukuPersistentVolumeNamespaceLister interface {
	// List lists all KukuPersistentVolumes in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*v1.KukuPersistentVolume, err error)
	// Get retrieves the KukuPersistentVolume from the indexer for a given namespace and name.
	Get(name string) (*v1.KukuPersistentVolume, error)
	KukuPersistentVolumeNamespaceListerExpansion
}

// kukuPersistentVolumeNamespaceLister implements the KukuPersistentVolumeNamespaceLister
// interface.
type kukuPersistentVolumeNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all KukuPersistentVolumes in the indexer for a given namespace.
func (s kukuPersistentVolumeNamespaceLister) List(selector labels.Selector) (ret []*v1.KukuPersistentVolume, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuPersistentVolume))
	})
	return ret, err
}

// Get retrieves the KukuPersistentVolume from the indexer for a given namespace and name.
func (s kukuPersistentVolumeNamespaceLister) Get(name string) (*v1.KukuPersistentVolume, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1.Resource("kukupersistentvolume"), name)
	}
	return obj.(*v1.KukuPersistentVolume), nil
}
