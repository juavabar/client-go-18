/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by lister-gen. DO NOT EDIT.

package v1

import (
	v1 "gitlab.com/juavabar/client-go-18/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// KukuSecretLister helps list KukuSecrets.
type KukuSecretLister interface {
	// List lists all KukuSecrets in the indexer.
	List(selector labels.Selector) (ret []*v1.KukuSecret, err error)
	// KukuSecrets returns an object that can list and get KukuSecrets.
	KukuSecrets(namespace string) KukuSecretNamespaceLister
	KukuSecretListerExpansion
}

// kukuSecretLister implements the KukuSecretLister interface.
type kukuSecretLister struct {
	indexer cache.Indexer
}

// NewKukuSecretLister returns a new KukuSecretLister.
func NewKukuSecretLister(indexer cache.Indexer) KukuSecretLister {
	return &kukuSecretLister{indexer: indexer}
}

// List lists all KukuSecrets in the indexer.
func (s *kukuSecretLister) List(selector labels.Selector) (ret []*v1.KukuSecret, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuSecret))
	})
	return ret, err
}

// KukuSecrets returns an object that can list and get KukuSecrets.
func (s *kukuSecretLister) KukuSecrets(namespace string) KukuSecretNamespaceLister {
	return kukuSecretNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// KukuSecretNamespaceLister helps list and get KukuSecrets.
type KukuSecretNamespaceLister interface {
	// List lists all KukuSecrets in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*v1.KukuSecret, err error)
	// Get retrieves the KukuSecret from the indexer for a given namespace and name.
	Get(name string) (*v1.KukuSecret, error)
	KukuSecretNamespaceListerExpansion
}

// kukuSecretNamespaceLister implements the KukuSecretNamespaceLister
// interface.
type kukuSecretNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all KukuSecrets in the indexer for a given namespace.
func (s kukuSecretNamespaceLister) List(selector labels.Selector) (ret []*v1.KukuSecret, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuSecret))
	})
	return ret, err
}

// Get retrieves the KukuSecret from the indexer for a given namespace and name.
func (s kukuSecretNamespaceLister) Get(name string) (*v1.KukuSecret, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1.Resource("kukusecret"), name)
	}
	return obj.(*v1.KukuSecret), nil
}
