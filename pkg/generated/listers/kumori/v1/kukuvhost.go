/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by lister-gen. DO NOT EDIT.

package v1

import (
	v1 "gitlab.com/juavabar/client-go-18/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// KukuVHostLister helps list KukuVHosts.
type KukuVHostLister interface {
	// List lists all KukuVHosts in the indexer.
	List(selector labels.Selector) (ret []*v1.KukuVHost, err error)
	// KukuVHosts returns an object that can list and get KukuVHosts.
	KukuVHosts(namespace string) KukuVHostNamespaceLister
	KukuVHostListerExpansion
}

// kukuVHostLister implements the KukuVHostLister interface.
type kukuVHostLister struct {
	indexer cache.Indexer
}

// NewKukuVHostLister returns a new KukuVHostLister.
func NewKukuVHostLister(indexer cache.Indexer) KukuVHostLister {
	return &kukuVHostLister{indexer: indexer}
}

// List lists all KukuVHosts in the indexer.
func (s *kukuVHostLister) List(selector labels.Selector) (ret []*v1.KukuVHost, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuVHost))
	})
	return ret, err
}

// KukuVHosts returns an object that can list and get KukuVHosts.
func (s *kukuVHostLister) KukuVHosts(namespace string) KukuVHostNamespaceLister {
	return kukuVHostNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// KukuVHostNamespaceLister helps list and get KukuVHosts.
type KukuVHostNamespaceLister interface {
	// List lists all KukuVHosts in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*v1.KukuVHost, err error)
	// Get retrieves the KukuVHost from the indexer for a given namespace and name.
	Get(name string) (*v1.KukuVHost, error)
	KukuVHostNamespaceListerExpansion
}

// kukuVHostNamespaceLister implements the KukuVHostNamespaceLister
// interface.
type kukuVHostNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all KukuVHosts in the indexer for a given namespace.
func (s kukuVHostNamespaceLister) List(selector labels.Selector) (ret []*v1.KukuVHost, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuVHost))
	})
	return ret, err
}

// Get retrieves the KukuVHost from the indexer for a given namespace and name.
func (s kukuVHostNamespaceLister) Get(name string) (*v1.KukuVHost, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1.Resource("kukuvhost"), name)
	}
	return obj.(*v1.KukuVHost), nil
}
