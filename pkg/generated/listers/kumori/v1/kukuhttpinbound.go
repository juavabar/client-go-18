/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by lister-gen. DO NOT EDIT.

package v1

import (
	v1 "gitlab.com/juavabar/client-go-18/pkg/apis/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/tools/cache"
)

// KukuHttpInboundLister helps list KukuHttpInbounds.
type KukuHttpInboundLister interface {
	// List lists all KukuHttpInbounds in the indexer.
	List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error)
	// KukuHttpInbounds returns an object that can list and get KukuHttpInbounds.
	KukuHttpInbounds(namespace string) KukuHttpInboundNamespaceLister
	KukuHttpInboundListerExpansion
}

// kukuHttpInboundLister implements the KukuHttpInboundLister interface.
type kukuHttpInboundLister struct {
	indexer cache.Indexer
}

// NewKukuHttpInboundLister returns a new KukuHttpInboundLister.
func NewKukuHttpInboundLister(indexer cache.Indexer) KukuHttpInboundLister {
	return &kukuHttpInboundLister{indexer: indexer}
}

// List lists all KukuHttpInbounds in the indexer.
func (s *kukuHttpInboundLister) List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error) {
	err = cache.ListAll(s.indexer, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuHttpInbound))
	})
	return ret, err
}

// KukuHttpInbounds returns an object that can list and get KukuHttpInbounds.
func (s *kukuHttpInboundLister) KukuHttpInbounds(namespace string) KukuHttpInboundNamespaceLister {
	return kukuHttpInboundNamespaceLister{indexer: s.indexer, namespace: namespace}
}

// KukuHttpInboundNamespaceLister helps list and get KukuHttpInbounds.
type KukuHttpInboundNamespaceLister interface {
	// List lists all KukuHttpInbounds in the indexer for a given namespace.
	List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error)
	// Get retrieves the KukuHttpInbound from the indexer for a given namespace and name.
	Get(name string) (*v1.KukuHttpInbound, error)
	KukuHttpInboundNamespaceListerExpansion
}

// kukuHttpInboundNamespaceLister implements the KukuHttpInboundNamespaceLister
// interface.
type kukuHttpInboundNamespaceLister struct {
	indexer   cache.Indexer
	namespace string
}

// List lists all KukuHttpInbounds in the indexer for a given namespace.
func (s kukuHttpInboundNamespaceLister) List(selector labels.Selector) (ret []*v1.KukuHttpInbound, err error) {
	err = cache.ListAllByNamespace(s.indexer, s.namespace, selector, func(m interface{}) {
		ret = append(ret, m.(*v1.KukuHttpInbound))
	})
	return ret, err
}

// Get retrieves the KukuHttpInbound from the indexer for a given namespace and name.
func (s kukuHttpInboundNamespaceLister) Get(name string) (*v1.KukuHttpInbound, error) {
	obj, exists, err := s.indexer.GetByKey(s.namespace + "/" + name)
	if err != nil {
		return nil, err
	}
	if !exists {
		return nil, errors.NewNotFound(v1.Resource("kukuhttpinbound"), name)
	}
	return obj.(*v1.KukuHttpInbound), nil
}
