/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	"context"

	getambassadoriov1 "gitlab.com/juavabar/client-go-18/pkg/apis_ambassador/getambassador.io/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeMappings implements MappingInterface
type FakeMappings struct {
	Fake *FakeGetambassadorV1
	ns   string
}

var mappingsResource = schema.GroupVersionResource{Group: "getambassador.io", Version: "v1", Resource: "mappings"}

var mappingsKind = schema.GroupVersionKind{Group: "getambassador.io", Version: "v1", Kind: "Mapping"}

// Get takes name of the mapping, and returns the corresponding mapping object, and an error if there is any.
func (c *FakeMappings) Get(ctx context.Context, name string, options v1.GetOptions) (result *getambassadoriov1.Mapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(mappingsResource, c.ns, name), &getambassadoriov1.Mapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.Mapping), err
}

// List takes label and field selectors, and returns the list of Mappings that match those selectors.
func (c *FakeMappings) List(ctx context.Context, opts v1.ListOptions) (result *getambassadoriov1.MappingList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(mappingsResource, mappingsKind, c.ns, opts), &getambassadoriov1.MappingList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &getambassadoriov1.MappingList{ListMeta: obj.(*getambassadoriov1.MappingList).ListMeta}
	for _, item := range obj.(*getambassadoriov1.MappingList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested mappings.
func (c *FakeMappings) Watch(ctx context.Context, opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(mappingsResource, c.ns, opts))

}

// Create takes the representation of a mapping and creates it.  Returns the server's representation of the mapping, and an error, if there is any.
func (c *FakeMappings) Create(ctx context.Context, mapping *getambassadoriov1.Mapping, opts v1.CreateOptions) (result *getambassadoriov1.Mapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(mappingsResource, c.ns, mapping), &getambassadoriov1.Mapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.Mapping), err
}

// Update takes the representation of a mapping and updates it. Returns the server's representation of the mapping, and an error, if there is any.
func (c *FakeMappings) Update(ctx context.Context, mapping *getambassadoriov1.Mapping, opts v1.UpdateOptions) (result *getambassadoriov1.Mapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(mappingsResource, c.ns, mapping), &getambassadoriov1.Mapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.Mapping), err
}

// Delete takes name of the mapping and deletes it. Returns an error if one occurs.
func (c *FakeMappings) Delete(ctx context.Context, name string, opts v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(mappingsResource, c.ns, name), &getambassadoriov1.Mapping{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeMappings) DeleteCollection(ctx context.Context, opts v1.DeleteOptions, listOpts v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(mappingsResource, c.ns, listOpts)

	_, err := c.Fake.Invokes(action, &getambassadoriov1.MappingList{})
	return err
}

// Patch applies the patch and returns the patched mapping.
func (c *FakeMappings) Patch(ctx context.Context, name string, pt types.PatchType, data []byte, opts v1.PatchOptions, subresources ...string) (result *getambassadoriov1.Mapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(mappingsResource, c.ns, name, pt, data, subresources...), &getambassadoriov1.Mapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.Mapping), err
}
