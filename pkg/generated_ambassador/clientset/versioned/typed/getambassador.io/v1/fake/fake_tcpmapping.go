/*
* Copyright 2020 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */
// Code generated by client-gen. DO NOT EDIT.

package fake

import (
	"context"

	getambassadoriov1 "gitlab.com/juavabar/client-go-18/pkg/apis_ambassador/getambassador.io/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	types "k8s.io/apimachinery/pkg/types"
	watch "k8s.io/apimachinery/pkg/watch"
	testing "k8s.io/client-go/testing"
)

// FakeTCPMappings implements TCPMappingInterface
type FakeTCPMappings struct {
	Fake *FakeGetambassadorV1
	ns   string
}

var tcpmappingsResource = schema.GroupVersionResource{Group: "getambassador.io", Version: "v1", Resource: "tcpmappings"}

var tcpmappingsKind = schema.GroupVersionKind{Group: "getambassador.io", Version: "v1", Kind: "TCPMapping"}

// Get takes name of the tCPMapping, and returns the corresponding tCPMapping object, and an error if there is any.
func (c *FakeTCPMappings) Get(ctx context.Context, name string, options v1.GetOptions) (result *getambassadoriov1.TCPMapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewGetAction(tcpmappingsResource, c.ns, name), &getambassadoriov1.TCPMapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.TCPMapping), err
}

// List takes label and field selectors, and returns the list of TCPMappings that match those selectors.
func (c *FakeTCPMappings) List(ctx context.Context, opts v1.ListOptions) (result *getambassadoriov1.TCPMappingList, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewListAction(tcpmappingsResource, tcpmappingsKind, c.ns, opts), &getambassadoriov1.TCPMappingList{})

	if obj == nil {
		return nil, err
	}

	label, _, _ := testing.ExtractFromListOptions(opts)
	if label == nil {
		label = labels.Everything()
	}
	list := &getambassadoriov1.TCPMappingList{ListMeta: obj.(*getambassadoriov1.TCPMappingList).ListMeta}
	for _, item := range obj.(*getambassadoriov1.TCPMappingList).Items {
		if label.Matches(labels.Set(item.Labels)) {
			list.Items = append(list.Items, item)
		}
	}
	return list, err
}

// Watch returns a watch.Interface that watches the requested tCPMappings.
func (c *FakeTCPMappings) Watch(ctx context.Context, opts v1.ListOptions) (watch.Interface, error) {
	return c.Fake.
		InvokesWatch(testing.NewWatchAction(tcpmappingsResource, c.ns, opts))

}

// Create takes the representation of a tCPMapping and creates it.  Returns the server's representation of the tCPMapping, and an error, if there is any.
func (c *FakeTCPMappings) Create(ctx context.Context, tCPMapping *getambassadoriov1.TCPMapping, opts v1.CreateOptions) (result *getambassadoriov1.TCPMapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewCreateAction(tcpmappingsResource, c.ns, tCPMapping), &getambassadoriov1.TCPMapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.TCPMapping), err
}

// Update takes the representation of a tCPMapping and updates it. Returns the server's representation of the tCPMapping, and an error, if there is any.
func (c *FakeTCPMappings) Update(ctx context.Context, tCPMapping *getambassadoriov1.TCPMapping, opts v1.UpdateOptions) (result *getambassadoriov1.TCPMapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewUpdateAction(tcpmappingsResource, c.ns, tCPMapping), &getambassadoriov1.TCPMapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.TCPMapping), err
}

// Delete takes name of the tCPMapping and deletes it. Returns an error if one occurs.
func (c *FakeTCPMappings) Delete(ctx context.Context, name string, opts v1.DeleteOptions) error {
	_, err := c.Fake.
		Invokes(testing.NewDeleteAction(tcpmappingsResource, c.ns, name), &getambassadoriov1.TCPMapping{})

	return err
}

// DeleteCollection deletes a collection of objects.
func (c *FakeTCPMappings) DeleteCollection(ctx context.Context, opts v1.DeleteOptions, listOpts v1.ListOptions) error {
	action := testing.NewDeleteCollectionAction(tcpmappingsResource, c.ns, listOpts)

	_, err := c.Fake.Invokes(action, &getambassadoriov1.TCPMappingList{})
	return err
}

// Patch applies the patch and returns the patched tCPMapping.
func (c *FakeTCPMappings) Patch(ctx context.Context, name string, pt types.PatchType, data []byte, opts v1.PatchOptions, subresources ...string) (result *getambassadoriov1.TCPMapping, err error) {
	obj, err := c.Fake.
		Invokes(testing.NewPatchSubresourceAction(tcpmappingsResource, c.ns, name, pt, data, subresources...), &getambassadoriov1.TCPMapping{})

	if obj == nil {
		return nil, err
	}
	return obj.(*getambassadoriov1.TCPMapping), err
}
